ARG SENTRY_IMAGE_TAG

FROM sentry:$SENTRY_IMAGE_TAG

#RUN pip install sentry-auth-google && \
#    echo '\nif 'GOOGLE_CLIENT_ID' in os.environ:\n\
#    GOOGLE_CLIENT_ID = env('GOOGLE_CLIENT_SECRET')\n\
#    GOOGLE_CLIENT_SECRET = env('GOOGLE_CLIENT_SECRET')\n\
#    SENTRY_OPTIONS['auth-google.client-id'] = env('GOOGLE_CLIENT_SECRET')\n\
#    SENTRY_OPTIONS['auth-google.client-secret'] = env('GOOGLE_CLIENT_SECRET')\n\
#    '\
#    >> /etc/sentry/sentry.conf.py

### https://github.com/siemens/sentry-auth-oidc
RUN pip install sentry-auth-oidc
#    echo '\nOIDC_CLIENT_ID = env('OIDC_CLIENT_ID')\nOIDC_CLIENT_SECRET = env('OIDC_CLIENT_SECRET')\nOIDC_SCOPE = env('OIDC_SCOPE') or "openid email"\nOIDC_DOMAIN = env('OIDC_DOMAIN')'\
#    >> /etc/sentry/sentry.conf.py

## Fix dependencies requirements
#ERROR: requests 2.18.4 has requirement urllib3<1.23,>=1.21.1, but you'll have urllib3 1.24.2 which is incompatible.
#ERROR: sentry 9.1.1 has requirement requests[security]<2.21.0,>=2.20.0, but you'll have requests 2.18.4 which is incompatible.
RUN pip install --upgrade --no-deps --force-reinstall "urllib3<1.23,>=1.21.1" "requests<2.21.0,>=2.20.0"
