#!/bin/bash

cat <<EOF >> /etc/sentry/sentry.conf.py
OIDC_CLIENT_ID = env('OIDC_CLIENT_ID')

OIDC_CLIENT_SECRET = env('OIDC_CLIENT_SECRET')

OIDC_SCOPE = env('OIDC_SCOPE') or "openid email"

OIDC_DOMAIN = env('OIDC_DOMAIN')
EOF
